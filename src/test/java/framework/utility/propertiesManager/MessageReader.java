package framework.utility.propertiesManager;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.testng.Assert.assertEquals;

public class MessageReader {

    /**
     * Create Locale Object using the parameters set in automation.properties
     * Load the resource Bundle based on the Locale
     * get the Message corresponding to the code
     */

    private static final AutProperties autTestProperties = AutProperties.getInstance();
    private static String language = autTestProperties.getProperty("locale.language");
    private static String country = autTestProperties.getProperty("locale.country");

    private static ResourceBundle resourceBundle = null;

    public static String getMessage(String code, Locale locale, Object... params) {
        locale = new Locale(language, country);

        if(resourceBundle == null){
            resourceBundle = ResourceBundle.getBundle("LocaleTest", locale);
        }

        String message = resourceBundle.getString(code).trim();

        if (params.length > 0) {
            message = MessageFormat.format(message, params);
        }
        return message;
    }



}
