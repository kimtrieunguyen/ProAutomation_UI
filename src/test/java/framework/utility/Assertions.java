package framework.utility;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.utility.reportManager.ScreenShot;

import java.io.IOException;

/**
 * Created by rahulrana on 13/02/18.
 */
public class Assertions {

    public static boolean verifyEqual(String actual, String expected, String message, ExtentTest t1) throws Exception {

        String[][] data = {{"Message", message}, {"Actual", actual}, {"Expected", expected}};
        Markup m = MarkupHelper.createTable(data);

        if(actual.equals(expected)){
            t1.pass(m);
            return true;
        }else{
            t1.fail(m);
            t1.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.captureScreen()).build());
        }
        return false;
    }
}
