package framework.utility.common;

import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Driver;

public class Navigation {

    private WebDriver driver;
    private static ExtentTest pageInfo;
    private static WebDriverWait wait;

    public static Navigation init(ExtentTest t1) {
        pageInfo = t1;
        wait = new WebDriverWait(DriverFactory.getDriver(), 10);
        return PageFactory.initElements(DriverFactory.getDriver(), Navigation.class);
    }

    /*
    Common Page objects like navigate, logout, etc
     */
    @FindBy(id = "nav-sync-tutorial")
    private WebElement syncLink;

    @FindBy(className = "sidebar-toggler")
    private WebElement sideBarToggle;

    @FindBy(id = "sync-parent-link")
    private WebElement syncExpand;

    @FindBy(id = "nav-sync-tutorial-1")
    private WebElement syncTest01;

    @FindBy(id = "nav-sync-tutorial-2")
    private WebElement syncTest02;

    @FindBy(id = "nav-sync-tutorial-3")
    private WebElement syncTest03;

    @FindBy(id = "nav-object-identification")
    private WebElement linkOrTest;


    public void toSyncExplicitWaitTest1() {
        leftNavigation(syncExpand, syncTest01);
        pageInfo.info("Navigate to Sync Test Explicit Wait 01");
    }

    public void toSyncExplicitWaitTest2() {
        leftNavigation(syncExpand, syncTest02);
        pageInfo.info("Navigate to Sync Test Explicit Wait 02");
    }

    public void toSyncTutorialFluentWaitTest() {
        leftNavigation(syncExpand, syncTest03);
        pageInfo.info("Navigate to Sync Test Fluent Wait");
    }

    public void toSyncObjectIdentificationTest() {
        leftNavigation(syncExpand, linkOrTest);
        pageInfo.info("Navigate to Demo Object Identification");
    }

    /**
     * Left Navigation
     *
     * @param parentLink
     * @param childLink
     */
    public void leftNavigation(WebElement parentLink, WebElement childLink) {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 12);
        wait.until(ExpectedConditions.elementToBeClickable(sideBarToggle)).click();
        wait.until(ExpectedConditions.elementToBeClickable(parentLink)).click();
        wait.until(ExpectedConditions.elementToBeClickable(childLink)).click();
       /* sideBarToggle.click();
        parentLink.click();
        pageInfo.info("Click on Parent Link - " + parentLink.getText());
        childLink.click();
        pageInfo.info("Click on Child Link - " + childLink.getText());*/

    }

}
