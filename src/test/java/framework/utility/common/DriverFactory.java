package framework.utility.common;

import framework.utility.globalConst.ConfigInput;
import framework.utility.propertiesManager.AutProperties;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

/**
 * Created by rahul.rana on 5/5/2017.
 */
public class DriverFactory {

    private static final String SAFARI = "safari";
    private static final String FIREFOX = "firefox";
    private static final String CHROME = "chrome";

    public static WebDriver driver;

    public static WebDriver getDriver() {

        if(driver == null){
            driver = createDriver();
            driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
            driver.manage().window().fullscreen();
        }

        return driver;
    }

    private static WebDriver createDriver() {
        String browser = AutProperties.getInstance().getProperty("browser.name");

        if (browser.equalsIgnoreCase(CHROME)) {
            System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
            driver = new ChromeDriver();
        }

        else if (browser.toLowerCase().equals(FIREFOX)) {
            /*
            Create Driver for Firefox
             */
        } else {
            /*
            Create Driver for Default Browser
             */
        }

        return driver;
    }
}
