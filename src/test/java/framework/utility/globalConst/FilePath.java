package framework.utility.globalConst;

import java.io.File;

/**
 * Created by rahul.rana on 5/7/2017.
 */
public class FilePath {
    public final static String basePath = new File("").getAbsolutePath();

    // APP DATA path
    public final static String testDataPath = basePath + "/src/test/resources/TestData/";
    public final static String FILE_DOWNLOAD_PATH = basePath + "\\downloads";


}
