package framework.features;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.User;
import framework.pageObjects.registration.Login_pg2;
import framework.utility.common.Assertion;
import framework.utility.common.Start;
import framework.utility.globalConst.ConfigInput;

/**
 * Created by rahul.rana on 5/11/2017
 */
public class UserManagement {
    private static ExtentTest pNode;

    public static UserManagement init(ExtentTest t1) throws Exception {
        pNode = t1;
        return new UserManagement();
    }

    public UserManagement initiateUserRegistration(User user) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateUserRegistration", ExtentColor.BLUE);
        pNode.info(m);

        try{
            // Open th Registration Page
            Start.openRegistrationPage();

            // Initiate User registration
            Login_pg2.init(pNode)
                    .setFullName(user.fullName)
                    .setAddress(user.address)
                    .setCity(user.city)
                    .setEmail(user.email)
                    .setGender(user.gender)
                    .setPassword(user.password)
                    .setPasswordAgain(user.password)
                    .checkAgree()
                    .clickOnButtonSubit();

            // verify user is successfully registered!
            /*if(ConfigInput.isAssert){
                Assertion.verifyActionMessageContain("user.registration.success",
                        "verify user is successfully registered", pNode);
            }*/
        }catch (Exception e){
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    public UserManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        pNode.info(m);

        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
       // ConfigInput.isAssert = false;
        return this;
    }
}
