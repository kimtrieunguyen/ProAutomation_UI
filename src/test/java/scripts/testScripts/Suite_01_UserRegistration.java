package scripts.testScripts;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.UserManagement;
import framework.utility.common.Assertion;
import framework.utility.globalConst.Constants;
import framework.utility.globalConst.FunctionalTag;
import org.testng.annotations.Test;
import scripts.TestInit;

/**
 * Created by rahulrana on 05/11/17.
 */
public class Suite_01_UserRegistration extends TestInit {

    /**
     * Test 01
     * User Registration
     * verify success user registration
     *
     * @throws Exception
     */
    @Test(priority = 0)
    public void test_01() throws Exception {
        // Create a test instance
        ExtentTest t1 = pNode.createNode("User Registration | Positive Case",
                "Verify successful User Registration")
                .assignCategory(FunctionalTag.USER_REGISTRATION);

        // Create a User Object that has to be registered
        User firstUser = new User(Constants.USR_F);

        // Verify user registration
        UserManagement.init(t1)
                .initiateUserRegistration(firstUser);
    }

    /**
     * Test 02
     * User Registration: Field Validation
     * verify success user registration
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void test_02() throws Exception {
        // Create a test instance
        ExtentTest t2 = pNode.createNode("User Registration | Field Validation",
                "Verify that password has to be minimum 6 character!")
                .assignCategory(FunctionalTag.USER_REGISTRATION, FunctionalTag.FIELD_VALIDATION);

        // Create a User Object that has to be registered
        User testUser = new User(Constants.USR_M);

        // set the password field to a new password less than  5 character
        testUser.password = "2r2d2";

        // Verify user registration
        UserManagement.init(t2)
                .startNegativeTest()
                .initiateUserRegistration(testUser);

    }

    /* 程度报告
            硒
    自动化
            测试
    性感的*/


}
