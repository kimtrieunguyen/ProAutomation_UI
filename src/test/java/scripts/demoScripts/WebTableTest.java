package scripts.demoScripts;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import framework.pageObjects.webTables.TestWebTable_pg1;
import framework.utility.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by rahulrana on 14/11/17.
 */
public class WebTableTest {

    private WebDriver driver;
    ExtentReports extent;

    /**
     * Get the Driver
     */
    @BeforeClass
    public void beforeClassMethod() {
        // initialize driver
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        // initialize reports
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("reports/extent.html");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
    }

    @Test
    public void webTableTest() {
        /*
        open the Test Application
         */
        driver.get("http://www.inventoryart.com/testapp/?page=webtable");

        /**
         * Table 1
         * Static Table
         *
         * Check if a string is present in the WebTable
         * fetch the row count, column count
         * click on link corresponding to a string in other column same row
         */
        TestWebTable_pg1 page = new TestWebTable_pg1(driver);

        // get row Count
        int rowCount = getRowCount(page.table1);

        // Get Columns
        int columnCount = getColumnCount(page.table1);

        // check if a string is present in webtable
        boolean isPresent = isStringPresentInCurrentTable(page.table1, "January 7, 2003");

        // Get Link Corresponding to a reference String
        WebElement link1 = getLinkFromStaticTable(page.table1, "Google Chrome");
        //link1.click();


        /**
         * Table 2
         * Dynamic Table
         *
         * click on link corresponding to a string in other column same row
         */
        WebElement link2 = getLinkFromDynamicTable(page.table2, "Opera", "Other Site");


        /**
         * Table 3
         * Check a checkbox corresponding to a string value in the same row other Column
         */
        WebElement checkBox = getCheckBox(page.table3, "Opera");
        checkBox.click();

    }

    /**
     * @param table
     * @return
     */
    public int getRowCount(WebElement table) {
        return table.findElements(By.tagName("tr")).size();
    }

    /**
     * description
     *
     * @param table
     * @return
     */
    public int getColumnCount(WebElement table) {
        return table.findElements(By.tagName("tr"))
                .get(0)
                .findElements(By.tagName("th")).size();
    }

    /**
     * @param table
     * @param text
     * @return
     */
    public boolean isStringPresentInCurrentTable(WebElement table, String text) {
        if (table.findElements(By.xpath(".//tr/td[contains(text(), '" + text + "')]")).size() > 0)
            return true;
        else
            return false;
    }

    /**
     * @param table
     * @param text
     * @return
     */
    public WebElement getLinkFromStaticTable(WebElement table, String text) {
        return table.findElement(By.xpath(".//tr/td[contains(text(), '" + text + "')]/ancestor::tr[1]/td/a"));
    }

    /**
     * @param table
     * @param text
     * @param headerName
     * @return
     */
    public WebElement getLinkFromDynamicTable(WebElement table, String text, String headerName) {
        List<WebElement> headerCells = table.findElements(By.tagName("th"));

        for (int i = 0; i < headerCells.size(); i++) {
            if (headerCells.get(i).getText().equals(headerName)) {
                int index = i + 1;
                return table.findElement(By.xpath(".//tr/td[contains(text(), '" + text + "')]/ancestor::tr[1]/td[" + index + "]/a"));
            }
        }
        return null;
    }

    /**
     *
     * @param table
     * @param text
     * @return
     */
    public WebElement getCheckBox(WebElement table, String text) {
        return table.findElement(By.xpath(".//tr/td[contains(text(), '" + text + "')]/ancestor::tr[1]/td/input[@type='checkbox']"));
    }


}
